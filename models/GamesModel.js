/*
* GamesModel.js
* Defines the properties of a device.
* Author: Juan Piaggio. 
*/

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

/**
* Model: Game
*/
  GamesSchema = new Schema ({
    title : { type: String, required: true, index: true, unique: true},
    image : { type: String, required: true },
    live  : { type: Boolean },
    device: [{
    	deviceNo	 : { type: Number },
    	manufacturer : { type: String, required: true },
    	osVer        : { type: String, required: true },
    	deviceCheckOut   : {
    		by		 	 : { type : String },
    		timeIn 	 	 : { type : Date },
    		timeOut  	 : { type : Date },
    		authCode	 : { type : String},
    		expires		 : { type : Date, expires: '10h'} 
        }
    }],
    browser: [{
    	name: { type: String },
    	version : { type: String }
    }]
  });

module.exports = mongoose.model('Games', GamesSchema);