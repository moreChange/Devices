/*
* GamesOrder.js
* Defines the properties of games living in the lobby.
* Author: Juan Piaggio. 
*/

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

/**
* Model: Game
*/
LobbySchema = new Schema ({
	gameName : { type: String, required: true, index: true, unique: true},
	imageName : { type: String, required: false },
	mid : { type: String, required: true },
	cid  : { type: String, required: true },
	lobbyName : { type : String, required: true },
	category: { type : String },
	orderNum: { type: Number }
});

module.exports = mongoose.model('Lobby', LobbySchema);