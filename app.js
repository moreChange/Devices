
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , http = require('http')
  , path = require('path')
  , request = require('request')
  , recentFeedItems = []
  , feed = require('rsj');

var app = module.exports = express();

// Middlewear
app.use(function(req, res, next){
    res.locals.vFogs = recentFeedItems[0];
    res.locals.hackerNews = recentFeedItems[1];
    res.locals.geekAppleNews = recentFeedItems[2];
    res.locals.mybb = recentFeedItems[3];
    next();
});

//app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.set('env', process.env.NODE_ENV || 'development');
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  //app.use(express.favicon());
  //app.use(express.logger('dev'));
  //app.use(express.bodyParser());
  //app.use(express.methodOverride());
  //app.use(app.router);
  //app.use(require('stylus').middleware(__dirname + '/public'));
  app.use(express.static(path.join(__dirname, 'public')));
  app.enable('trust proxy');

//});

var processFeeds = function(callback){

  // V updates
  feed.r2j('https://fogbugz/default.asp?pg=pgRss&ixWikiPage=402&sSignature=697-3f2Y0JAFr5tpT9OBFwor7p8CqDk', function(data) {
    recentFeedItems[0] = JSON.parse(data);
    console.log('1');
  }); 

  // Hacker News
  feed.r2j('https://news.ycombinator.com/rss', function(data) {
    recentFeedItems[1] = JSON.parse(data);
    console.log('2');
  }); 

  //'http://www.geek.com/feed/' < -- Main
  //'http://www.geek.com/articles/mobile/feed/' <-- Mobile News
  //'http://www.geek.com/articles/android/feed/' <-- Android Feed
  //'http://www.geek.com/articles/apple/feed/'

  // Geek
  feed.r2j('http://www.geek.com/articles/apple/feed/', function(data) {
    recentFeedItems[2] = JSON.parse(data); 
    console.log('3');
  }); 

  // MyBB
  feed.r2j('http://mybroadband.co.za/news/feed', function(data) {
    recentFeedItems[3] = JSON.parse(data);
    
    console.log('4');
  }); 
  
      
  console.log('RETURNING');
  if (callback) callback();
}

// Routes
app.get('/', routes.index);
app.get('/feeds', routes.feeds);
//app.get('/feeds/apple', routes.feedsApple);
//app.get('/launch', routes.launch);
//app.get('/game-comp', routes.device);
app.get('/paylines', routes.paylines);
//app.post('/game-post', routes.game_post);
//app.get('/lobby', routes.lobby);
//app.post('/lobby-post',routes.lobby_post);

//setInterval(processFeeds, 18000); // process feeds items every 30 minutes

/*processFeeds(function() {
    app.listen(8000);
    console.log('Express started on port 8000');   
});*/

http.createServer(app).listen(app.get('port'), function(){
    console.log("Express server listening on port " + app.get('port'));
});

