function defineModels(mongoose, fn) {
  var Schema = mongoose.Schema,
      ObjectId = Schema.ObjectId;

  /**
    * Model: Device
    */
  Devices = new Schema({
    'manufacturer': { type: String, index: true },
    'osVer': String,
    'deviceNo': Number,
    'user_id': ''
  });

  /**
    * Model: Game
    */
  Games = new Schema ({
    'title': { type: String, index: true },
    'live': Boolean,
  });

  /**
    * Model: Browser
    */
  Browsers = new Schema({
    'vendor': String,
    'version' : [String]
  });

  mongoose.model('Devices', Devices);
  mongoose.model('Games', Games);
  mongoose.model('Browsers', Browsers);

  fn();
}

exports.defineModels = defineModels; 