var mongo = require('mongodb');

var Server = mongo.Server,
    Db = mongo.Db,
    BSON = mongo.BSONPure;

var server = new Server('localhost', 27017, {auto_reconnect: true});
db = new Db('lobbyOrder', server, {safe: true});

db.open(function(err, db) {
    populateDB();
    if(!err) {
        console.log("Connected to 'lobbyOrder' database");
        db.collection('devices', {safe:true}, function(err, collection) {
            console.log(err);
            if (err) {
                console.log("The 'devices' collection doesn't exist. Creating it with sample data...");
                populateDB();
            }
        });
    }
});

/*--------------------------------------------------------------------------------------------------------------------*/
// Populate database with sample data -- Only used once: the first time the application is started.
// You'd typically not find this code in a real-life app, since the database would already exist.
var populateDB = function() {

    /*var games = [{
        title : 'Major Millions',
        image : 'images/majormillions.png',
        live  : true,
        device : [{
            deviceNo: 10,
            manufacturer : 'Apple',
            osVer        : '5.1',
        },
        {
            deviceNo: 35,
            manufacturer : 'Android',
            osVer        : '4.0.4',
        }

        ]
    }]
    db.collection('games', function(err, collection) {
        collection.insert(games, {safe:true}, function(err, result) {});
    });*/

    var lobbyGames = [
    {
        gameName : 'Couch Potato',
        imageName: 'couchPotato',
        mid : '10024',
        cid  : '40301',
        lobbyName : 'Generic',
        category: 'main',
        orderNum: 0
    },
    {
        gameName : 'Pure Platinum',
        imageName: 'purePlatinum',
        mid : '12671',
        cid  : '40300',
        lobbyName : 'Generic',
        category: 'main',
        orderNum: 1
    },
    {
        gameName : '5 Reel Drive',
        imageName: '5reeldrive',
        mid : '10008',
        cid  : '40301',
        lobbyName : 'Generic',
        category: 'main',
        orderNum: 2
    },
    {
        gameName : 'Avalon',
        imageName: 'avalon',
        mid : '10044',
        cid  : '40300',
        lobbyName : 'Generic',
        category: 'main',
        orderNum: 3
    },
    {
        gameName : 'Tomb Raider',
        imageName : 'tombRaider',
        mid : '12506',
        cid  : '40302',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 4
    },
    {
        gameName : 'Lion\'s Pride',
        imageName : 'lionsPride',
        mid : '11144',
        cid  : '40300',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 5
    },
    {
        gameName : 'Break Da Bank',
        imageName : 'breakDaBank',
        mid : '10001',
        cid  : '40300',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 6
    },
    {
        gameName : 'Big Top',
        imageName : 'bigTop',
        mid : '10020',
        cid  : '40300',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 7
    },
    {
        gameName : 'Carnaval',
        imageName : 'carnaval',
        mid : '10020',
        cid  : '40301',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 8
    },
    {
        gameName : 'European Roulette Gold',
        imageName : 'europeanRoulette',
        mid : '2',
        cid  : '40300',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 9
    },
    {
        gameName : 'European Blackjack Gold',
        imageName : 'europeanBlackjackGold',
        mid : '134',
        cid  : '40300',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 10
    },
    {
        gameName : 'Mega Moolah',
        imageName : 'megaMoolah',
        mid : '17501',
        cid  : '40300',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 11
    },
    {
        gameName : 'Thunderstruck',
        imageName : 'thunderstruck',
        mid : '10025',
        cid  : '40300',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 12
    },
    {
        gameName : 'Voila',
        imageName : 'voila',
        mid : '11145',
        cid  : '40300',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 13
    },
    {
        gameName : 'Burning Desire',
        imageName : 'burningDesire',
        mid : '11016',
        cid  : '40300',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 14
    },
    {
        gameName : 'Stash Of The Titans',
        imageName : 'stashOfTheTitans',
        mid : '11014',
        cid  : '40300',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 15
    },
    {
        gameName : 'Break Da Bank Again',
        imageName : 'breakDaBankAgain',
        mid : '11004',
        cid  : '40300',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 16
    },
    {
        gameName : 'Alaskan Fishing',
        imageName : 'alaskanFishing',
        mid : '12702',
        cid  : '40300',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 17
    },
    {
        gameName : 'Mermaids Millions',
        imageName : 'mermaidsMillions',
        mid : '12506',
        cid  : '40300',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 18
    },
    {
        gameName : 'Cashapillar',
        imageName : 'cashapillar',
        mid : '11006',
        cid  : '40300',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 19
    },
    {
        gameName : 'Loaded',
        imageName : 'loaded',
        mid : '12528',
        cid  : '40300',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 20
    },
    {
        gameName : 'Agent Jane Blonde',
        imageName : 'agentJaneBlonde',
        mid : '10025',
        cid  : '40300',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 21
    },
    {
        gameName : 'Adventure Palace',
        imageName : 'adventurePalace',
        mid : '10025',
        cid  : '40305',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 22
    },
    {
        gameName : 'Spring Break',
        imageName : 'springBreak',
        mid : '10025',
        cid  : '40301',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 23
    },
    {
        gameName : 'Deck The Halls',
        imageName : 'deckTheHalls',
        mid : '11020',
        cid  : '40301',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 24
    },
    {
        gameName : 'Ladies Nite',
        imageName : 'ladiesNite',
        mid : '10025',
        cid  : '40304',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 25
    },
    {
        gameName : 'Tally Ho',
        imageName : 'tallyHo',
        mid : '10038',
        cid  : '40304',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 26
    },
    {
        gameName : 'Major Millions',
        imageName : 'majorMillions',
        mid : '15005',
        cid  : '40300',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 27
    },
    {
        gameName : 'Cash Splash',
        imageName : 'cashSplash',
        mid : '15004',
        cid  : '40300',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 28
    },
    {
        gameName : 'Treasure Nile',
        imageName : 'treasureNile',
        mid : '15001',
        cid  : '40300',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 29
    },
    {
        gameName : 'Jacks Or Better Poker',
        imageName : 'jacksOrBetter',
        mid : '6',
        cid  : '40300',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 30
    },
    {
        gameName : 'Double Double Bonus Poker',
        imageName : 'doubleDoubleBonus',
        mid : '85',
        cid  : '40300',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 31
    },
    {
        gameName : 'Bonus Deuces Wild Poker',
        imageName : 'bonusDeucesWild',
        mid : '90',
        cid  : '40300',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 32
    },
    {
        gameName : 'Deuces Wild Poker',
        imageName : 'deucesWild',
        mid : '17',
        cid  : '40300',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 33
    },
    {
        gameName : 'Aces And Eights Poker',
        imageName : 'acesAndEights',
        mid : '88',
        cid  : '40300',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 34
    },
    {
        gameName : 'Aces And Faces',
        imageName : 'acesAndFaces',
        mid : '27',
        cid  : '40300',
        lobbyName : 'Generic',
        category: 'Main',
        orderNum: 35
    }

    ]

    db.collection('lobbies', function(err, collection) {
        collection.save(lobbyGames, { upsert: true }, function(err, result) {});
    });

};