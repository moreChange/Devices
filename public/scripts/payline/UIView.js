﻿(function (global) {

    var paylineGeneratorNamespace = global.mgs.mobileAutomations.common.Utils.resolveNamespace('mgs.mobileAutomations.paylineGenerator');
    var commonUtils = global.mgs.mobileAutomations.common.Utils.resolveNamespace('mgs.mobileAutomations.common.Utils');

    function uiView() {
        this.setUp();
    };

    var _p = uiView.prototype;

    _p.setUp = function () {
        this.setDisplay();
        this.setupVisArea();

    };

    _p.setDisplay = function (){
        $('#sideBar').css('height', (screen.availHeight - $('#Header').height() - 200) + 'px');
        $('#sideBar').css('width', (screen.availWidth / 4) + 'px');
        $('#bodyContent').css('height', (screen.availHeight - $('#Header').height()) + 'px');
        $('#bodyContent').css('width', (((screen.availWidth / 4) * 3) - 70) + 'px');
        $('#paylineConfigTxt').css('width', $('#sideBar').width() - 20 + 'px');
        $('#paylineConfigTxt').css('height', $('#sideBar').height() / 2 + 'px');
    };

    _p.setupVisArea = function () {
        $("#VisArea").hide();
        
        $('#view').submit(commonUtils.createDelegate(this, function (e) {
            e.preventDefault();
            paylineGeneratorNamespace.paylineGeneratorUtils.clearTextArea();
            $("#VisArea div").remove();
            var data = {
                rows: parseInt(e.target[0].value),
                columns: parseInt(e.target[1].value),
                width: 82,
                heigth: 82,
                reelDiv: 7
            };
            var setDisplay = new paylineGeneratorNamespace.setDisplayArea(data);
            setDisplay.CalculateVisArea('VisArea');
            setDisplay.CalculateSymbolSize('VisArea');
        }));
    };
    paylineGeneratorNamespace.uiView = new uiView();

})(window);