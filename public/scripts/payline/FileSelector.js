﻿(function (global) {

    var paylineGeneratorNamespace = global.mgs.mobileAutomations.common.Utils.resolveNamespace('mgs.mobileAutomations.paylineGenerator');
    var commonUtils = global.mgs.mobileAutomations.common.Utils.resolveNamespace('mgs.mobileAutomations.common.Utils');

    function FileSelector() {
        this._indexs = [];
        this._visibleArea = {};
        this.setUp();
    };

    var _p = FileSelector.prototype;

    _p.setUp = function () {
        var _config = paylineGeneratorNamespace.config;
        $('#chooseFile').change(commonUtils.createDelegate(this, function (e) {
            var files = e.target.files; // FileList object
            // Loop through the FileList
            for (var q = 0, f; f = files[q]; q++) {
                if (!f.type.match('xml.*')) {
                    alert("XML PLEASE");
                    continue;
                }
                var reader = new FileReader();
                reader.onload = commonUtils.createDelegate(this, this.createPaylinefromConfig);
                var t = reader.readAsText(f);
            }
        }));

        $('#fileSubmit').click(commonUtils.createDelegate(this , this.handleFileSubmit));
    };

    _p.createPaylinefromConfig = function (evt) {
        this._file = {};
        this._visibleArea.columns = parseInt($(evt.target.result).find('VisibleArea').attr('width'));
        this._visibleArea.rows = parseInt($(evt.target.result).find('VisibleArea').attr('height'));
        this._file = evt.target.result;
    };

    _p.handleFileSubmit = function () {
        if (this._file !== undefined) {
            if (!$.isEmptyObject(paylineGeneratorNamespace.setupCheckBox._checkBoxObject)) {
                paylineGeneratorNamespace.paylineGeneratorUtils.clearTextArea();
                var count = 0;
                var that = this;
                $(this._file).find('Payline').each(function () {
                    that._indexs['Payline' + count] = $(this).attr("indexes").split(',');
                    count++;
                });
                paylineGeneratorNamespace.setupCheckBox.getResDimensions();
                this.createPointsforResolution();
                this.mapCoandPaylines();
            } else {
                alert("Please Select a Resolution!");
            }
            } else {
            alert("PLease Select an Xml!");
        }
    };

    _p.createPointsforResolution = function () {
        this.allResPoints = {};
        for (var x in paylineGeneratorNamespace.setupCheckBox._checkBoxObject){
            this.allResPoints[x] = paylineGeneratorNamespace.paylineGeneratorUtils.CalculateResolutionSizes(x, this._visibleArea)[x];
        };
    };

    _p.mapCoandPaylines = function () {
        for (x in paylineGeneratorNamespace.setupCheckBox._checkBoxObject) {
            var count = 0;
            for (var y in this._indexs) {
                var payline = paylineGeneratorNamespace.paylineGeneratorUtils.sortPaylineObjectFromSymbolArray(x, this._indexs[y]);
                paylineGeneratorNamespace.paylineGeneratorUtils.displayTxtCo(payline, x, count);
                count++;
            }
        }
    };
    paylineGeneratorNamespace.fileSelector = new FileSelector();

})(window);