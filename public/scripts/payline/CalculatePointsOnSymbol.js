﻿(function(global) {
    var paylineGeneratorNamespace = global.mgs.mobileAutomations.common.Utils.resolveNamespace('mgs.mobileAutomations.paylineGenerator');
    var commonUtils = global.mgs.mobileAutomations.common.Utils.resolveNamespace('mgs.mobileAutomations.common.Utils');

    function CalculatePointOnSymbol() {

    }

    var _p = CalculatePointOnSymbol.prototype;

    _p.CreateRadioButton = function(data) {
        var radioInput = document.createElement('div');
        radioInput.id = data.id + ',point1';
        radioInput.style.top = data.y + data.height / 2 + 'px';
        radioInput.style.left = data.x + 'px';
        radioInput.className = 'radioPoint';
        radioInput.style.visibility = '';
        document.getElementById('VisArea').appendChild(radioInput);

        radioInput = document.createElement('div');
        radioInput.id = data.id + ',point2';
        radioInput.style.top = data.y + data.height / 2 + 'px';
        radioInput.style.left = data.x + data.width / 2 + 'px';
        radioInput.className = 'radioPoint';
        radioInput.style.visibility = '';
        document.getElementById('VisArea').appendChild(radioInput);
        
        radioInput = document.createElement('div');
        radioInput.id = data.id + ',point3';
        radioInput.style.top = data.y + data.height / 2 + 'px';
        radioInput.style.left = data.x + data.width + 'px';
        radioInput.className = 'radioPoint';
        radioInput.style.visibility = '';
        document.getElementById('VisArea').appendChild(radioInput);
    };

    paylineGeneratorNamespace.calculatePointOnSymbol = new CalculatePointOnSymbol();

}(window));