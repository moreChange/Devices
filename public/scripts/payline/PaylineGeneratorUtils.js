﻿(function (global) {

    var paylineGeneratorNamespace = global.mgs.mobileAutomations.common.Utils.resolveNamespace('mgs.mobileAutomations.paylineGenerator');
    var commonUtils = global.mgs.mobileAutomations.common.Utils.resolveNamespace('mgs.mobileAutomations.common.Utils');

    function PaylineGeneratorUtils() {
        this.mappingSymbols = {};

    };

    _p = PaylineGeneratorUtils.prototype;

    _p.CalculateResolutionSizes = function (dta, visArea) {
        var countsymbol = 0,
            x = 0,
            y = 0,
            ResObject = {},
            useData = paylineGeneratorNamespace.setupCheckBox._finalResDimensions[dta];
        ResObject[dta] = {};
        for (var i = 0 ; i < visArea.columns ; i++) {
            y = 0;
            var rowCount = visArea.rows - 1;
            for (var k = 0 ; k < visArea.rows ; k++) {
                countsymbol++;
                var data = {};
                data.name = 'symbol' + (rowCount * visArea.columns + i);
                //data.id = 'symbol' + (rowCount * visArea.columns + i);
                data.id = 'column' + i + ',symbol' + k;
                data.y = y;
                data.x = x;
                data.width = useData.symbolWidth;
                data.height = useData.symbolHeight;
                this.mappingSymbols[data.name] = data.id;
                this.CreateResPoints(dta ,data, ResObject);
                y += useData.symbolHeight;
                rowCount--;
            }
            x += useData.symbolWidth;
            x += useData.reelDiv;
        }
        console.log(this.mappingSymbols);
        return ResObject;
    };

    _p.CreateResPoints = function (dta, data , obj) {
        obj[dta][data.id + ',point1'] = {
            y: data.y + data.height / 2,
            x: data.x
        };

        obj[dta][data.id + ',point2'] = {
            y: data.y + data.height / 2,
            x: data.x + data.width / 2
        };

        obj[dta][data.id + ',point3'] = {
            y: data.y + data.height / 2,
            x: data.x + data.width
        };
    };

    _p.sortPaylineObjectFromSymbolArray = function (resolution , data) {
            var paylineObject = {};
            tempstr1 = this.mappingSymbols['symbol' + data[0]] + ',point1';
            console.log('tempstr1 = ' + tempstr1);
            paylineObject[tempstr1] = paylineGeneratorNamespace.fileSelector.allResPoints[resolution][tempstr1];
            for (var l = 0 ; l < data.length - 1; l++) {
                var res = this.getSymbol(this.mappingSymbols['symbol' + data[l + 1]]) -
                    this.getSymbol(this.mappingSymbols['symbol' + data[l]])
                //parseInt(this.mappingSymbols['symbol' + this.data[l + 1]]) - parseInt(this.mappingSymbols['symbol' + this.data[0]]);
                var firstCol = this.mappingSymbols['symbol' + data[l]];
                var secCol = this.mappingSymbols['symbol' + data[l+1]];
                console.log('firstCol = ' + firstCol);
                console.log('secCol = ' + secCol);
                var tempstr1,
                    tempstr2,
                    tempstr3,
                    tempstr4;

                if (res === 0) {
                    tempstr1 = firstCol + ',point2';
                    paylineObject[tempstr1] = 
                        paylineGeneratorNamespace.fileSelector.allResPoints[resolution][tempstr1];
                    console.log(paylineGeneratorNamespace.fileSelector.allResPoints[resolution][tempstr1]);
                    tempstr2 = secCol + ',point2';
                    paylineObject[tempstr2] = paylineGeneratorNamespace.fileSelector.allResPoints[resolution][tempstr2];
                } else {
                    if (res === -1 || res === 1) {
                        tempstr1 = firstCol + ',point2';
                        paylineObject[tempstr1] = paylineGeneratorNamespace.fileSelector.allResPoints[resolution][tempstr1];
                        tempstr2 = secCol + ',point2';
                        paylineObject[tempstr2] = paylineGeneratorNamespace.fileSelector.allResPoints[resolution][tempstr2];
                    } else {
                        tempstr1 = firstCol + ',point2';
                        paylineObject[tempstr1] = paylineGeneratorNamespace.fileSelector.allResPoints[resolution][tempstr1];
                        tempstr2 = firstCol + ',point3';
                        paylineObject[tempstr2] = paylineGeneratorNamespace.fileSelector.allResPoints[resolution][tempstr2];
                        tempstr3 = secCol + ',point1';
                        paylineObject[tempstr3] = paylineGeneratorNamespace.fileSelector.allResPoints[resolution][tempstr3];
                        tempstr4 = secCol + ',point2';
                        paylineObject[tempstr4] = paylineGeneratorNamespace.fileSelector.allResPoints[resolution][tempstr4];
                    }
                }
            }

            tempstr1 = this.mappingSymbols['symbol' + data[data.length - 1]] + ',point3';
            paylineObject[tempstr1] = paylineGeneratorNamespace.fileSelector.allResPoints[resolution][tempstr1];
            console.log(paylineObject);

            var keys = Object.keys(paylineObject);

            var payline = {};
            payline.points = [];
            var typeMove = {};
            typeMove.type = 'Move';
            typeMove.point = [paylineObject[keys[0]]];
            payline.points.push(typeMove);

            var typeLine = {};
            typeLine.type = 'Line';
            typeLine.point = [];

            for (i = 1 ; i < keys.length ; i++) {
                typeLine.point.push(paylineObject[keys[i]]);
            }
            payline.points.push(typeLine);

        // this.drawPayLine(payline);
            //this.displayTxtCo(payline, data);
            return payline;
    };
    _p.getSymbol = function (data) {
        var temp = data.split(','),
            res = parseInt(temp[1].substring(6));
        return res;
    };

    _p.displayTxtCo = function (data, res , num) {
        var txt = 'payline' + num + ' : {\n';
        txt += '   points : [\n';
        var len = data.points.length;
        for (var i = 0; i < len; i++) {
            var dta = data.points[i];
            switch (dta.type) {
                case 'Move':
                    txt += '                { type : \"Move\" , point: [{x: ' + dta.point["0"].x + ', y:' + dta.point["0"].y + '}]},\n';
                    break;
                default:
                    txt += '                { type : \"Line\" , point: [';
                    for (var pointSet in dta.point) {
                        txt += '{ x:' + dta.point[pointSet].x + ', y:' + dta.point[pointSet].y + ' },';
                    }
                    break;
            }
        }
        txt = txt.slice(0, txt.length - 1);
        txt += '] }';
        txt += '\n              ]},';
        $('#textArea' + res).val($('#textArea' + res).val() + '\n' + txt);
    };

    _p.clearTextArea = function () {
        var key = $('#display label textarea');
        for (var a = 0; a < key.length ; a++) {
            $(key[a]).val('');
        }
    };
    paylineGeneratorNamespace.paylineGeneratorUtils = new PaylineGeneratorUtils();
}(window));