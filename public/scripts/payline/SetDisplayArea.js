﻿(function (global) {
    var paylineGeneratorNamespace = global.mgs.mobileAutomations.common.Utils.resolveNamespace('mgs.mobileAutomations.paylineGenerator');
        var commonUtils = global.mgs.mobileAutomations.common.Utils.resolveNamespace('mgs.mobileAutomations.common.Utils');

    function SetDisplayArea(data) {
        this.setDisplayAreaData = new Object();
        this.setDisplayAreaData.rows = data.rows;
        this.setDisplayAreaData.columns = data.columns;
        this.setDisplayAreaData.symbolWidth = data.width;
        this.setDisplayAreaData.symbolHeight = data.heigth;
        this.setDisplayAreaData.reelDiv = data.reelDiv;
        this.setDisplayAreaData.width = (data.columns * data.width) + (data.reelDiv * (data.columns - 1));
        this.setDisplayAreaData.Height = data.heigth * data.rows;
        this.highlightSymbol = new Array();
        this.setDisplayAreaData.symbolDimensions = new Object();
        this.paylineObject = {};
        this.globalResOject = {};
            document.getElementById('calpayline').onclick = commonUtils.createDelegate(this, this.sortPaylineObjectFromSymbolArray);
    }

    var _p = SetDisplayArea.prototype;

    /**
    * My method description : This method is used to calculate the VisArea width and height.
    *
    * @method CalculateVisArea
    * @param {String} vArea id of Visarea to set.
    */
    _p.CalculateVisArea = function(vArea) {
        
       $('#' + vArea).css('height', this.setDisplayAreaData.Height + 'px');
        $('#' + vArea).css('width', this.setDisplayAreaData.width + 'px');
        $('#' + vArea).css('left', (screen.availWidth - this.setDisplayAreaData.width) / 2 + 'px');
        $('#paylineCanvas').css('height', this.setDisplayAreaData.Height + 'px');
        $('#paylineCanvas').css('width', this.setDisplayAreaData.width + 'px');
        $('#paylineCanvas').attr('height', this.setDisplayAreaData.Height);
        $('#paylineCanvas').attr('width', this.setDisplayAreaData.width);
        $('#resetBtn').css('top', this.setDisplayAreaData.Height + 'px');
        $('#calpayline').css('top', this.setDisplayAreaData.Height + 'px');
        $('#display').css('top', (this.setDisplayAreaData.Height + 200) + 'px');
    };

    /**
    * My method description : This method is used to calculate the VisArea width and height.
    *
    * @method CalculateVisArea
    * @param {String} vArea id of Visarea to set.
    */
    _p.CalculateSymbolSize = function (vArea) {
        $("#VisArea").show();
        var countsymbol = 0,
            x = 0,
            y = 0;
        global.TweenMax.from($('#VisArea'), 1, {
            scale: 0.0
        });
        for (var i = 0 ; i < this.setDisplayAreaData.columns ; i++) {
            y = 0;
            for (var k = 0 ; k < this.setDisplayAreaData.rows ; k++) {
                countsymbol++;
                var div = document.createElement('div');
                div.name = 'symbol' + countsymbol;
                div.id = 'column' + i + ',symbol' + k;
                div.style.position = 'absolute';
                div.style.top = y + 'px';
                div.style.left = x + 'px';
                div.style.width = this.setDisplayAreaData.symbolWidth + 'px';
                div.style.height = this.setDisplayAreaData.symbolHeight + 'px';
                if (countsymbol % 2 === 1) {
                    div.className = 'box';
                } else {
                    div.className = 'box1';
                }
                
                div.onclick = commonUtils.createDelegate(this, this.DealWithSymbolClick);
                $('#' + vArea).append(div);
                var data = {};
                data.id = div.id;
                data.width = this.setDisplayAreaData.symbolWidth;
                data.height = this.setDisplayAreaData.symbolHeight;
                data.x = x;
                data.y = y;
                paylineGeneratorNamespace.calculatePointOnSymbol.CreateRadioButton(data);
                global.TweenMax.from(div, 1, {
                    scale: 0.0
                });
                y += this.setDisplayAreaData.symbolHeight;
            }
            x += this.setDisplayAreaData.symbolWidth;
            x += this.setDisplayAreaData.reelDiv;
        }
        document.getElementById('resetBtn').onclick = commonUtils.createDelegate(this, function () {
            var canvas = document.getElementById('paylineCanvas');
            var offscreencontext = canvas.getContext("2d");
            offscreencontext.clearRect(0, 0, canvas.width, canvas.height);
            for (var a = 0 ; a < this.setDisplayAreaData.columns ; a++) {
                for (var b = 0 ; b < this.setDisplayAreaData.rows ; b++) {
                    $(document.getElementById('column' + a + ',symbol' + b)).css('opacity', '');
                }
            }
            this.highlightSymbol = new Array();
        });
    };

    _p.DealWithSymbolClick = function (arg) {
        arg.stopPropagation();
        var symbol = this.getColumnRow(arg.srcElement.id);
        if (this.highlightSymbol[symbol.column] === undefined) {
            $(arg.srcElement).css('opacity', '1');
            this.highlightSymbol[symbol.column] = symbol.symbol;
        } else {
            $(document.getElementById('column' + symbol.column + ',symbol' + this.highlightSymbol[symbol.column])).css('opacity', '');
            $(arg.srcElement).css('opacity', '1');
            this.highlightSymbol[symbol.column] = symbol.symbol;
        }
    };

    _p.getColumnRow = function (arg) {
        var crArray = arg.split(',');
        var result = {};
        result.column = parseInt(crArray[0].substring(6));
        result.symbol = parseInt(crArray[1].substring(6));

        return result;
    };

    _p.showSymbolPoints = function (arg) {
        for (var i = 0; i < 3; i++) {
            document.getElementById(arg + ',point' + (i + 1)).style.visibility = '';
            document.getElementById(arg + ',point' + (i + 1)).onclick = commonUtils.createDelegate(this, this.capturePoints);
        }
    };
    
    _p.hideSymbolPoints = function (arg) {
        for (var i = 0; i < 3; i++) {
            document.getElementById(arg + ',point' + (i + 1)).style.visibility = 'hidden';
            document.getElementById(arg + ',point' + (i + 1)).onclick = '';
            document.getElementById(arg + ',point' + (i + 1)).style.backgroundColor = '';
        }
    };

    _p.capturePoints = function (arg) {
        document.getElementById(arg.srcElement.id).style.backgroundColor = 'black';
        document.getElementById(arg.srcElement.id).onclick = '';
        document.getElementById(arg.srcElement.id).onclick = commonUtils.createDelegate(this, this.addHandlerBack);
        this.paylineObject[arg.srcElement.id] = {
            x: arg.srcElement.offsetLeft,
            y: arg.srcElement.offsetTop
        };
    };

    _p.addHandlerBack = function (arg) {
        document.getElementById(arg.srcElement.id).style.backgroundColor = '';
        document.getElementById(arg.srcElement.id).onclick = '';
        document.getElementById(arg.srcElement.id).onclick = commonUtils.createDelegate(this, this.capturePoints);
        delete this.paylineObject[arg.srcElement.id];
    };

    _p.sortPaylineObjectFromSymbolArray = function () {
        var run = !$.isEmptyObject(paylineGeneratorNamespace.setupCheckBox._checkBoxObject);
        if (this.highlightSymbol.length === this.setDisplayAreaData.columns) {
            if (run) {
                this.paylineObject = {};
                tempstr1 = 'column0,symbol' + this.highlightSymbol[0] + ',point1';
                this.paylineObject[tempstr1] = {
                    x: parseInt($(document.getElementById(tempstr1)).css('left'), 10),
                    y: parseInt($(document.getElementById(tempstr1)).css('top'), 10)
                };
                for (var l = 0 ; l < this.highlightSymbol.length - 1; l++) {
                    var res = this.highlightSymbol[l + 1] - this.highlightSymbol[l];
                    var firstCol = 'column' + l + ',symbol' + this.highlightSymbol[l];
                    var secCol = 'column' + (l + 1) + ',symbol' + this.highlightSymbol[l + 1];
                    var tempstr1,
                        tempstr2,
                        tempstr3,
                        tempstr4;

                    if (res === 0) {
                        tempstr1 = firstCol + ',point2';
                        this.paylineObject[tempstr1] = {
                            x: parseInt($(document.getElementById(tempstr1)).css('left'), 10),
                            y: parseInt($(document.getElementById(tempstr1)).css('top'), 10)
                        };
                        tempstr2 = secCol + ',point2';
                        this.paylineObject[tempstr2] = {
                            x: parseInt($(document.getElementById(tempstr2)).css('left'), 10),
                            y: parseInt($(document.getElementById(tempstr2)).css('top'), 10)
                        };
                    } else {
                        if (res === -1 || res === 1) {
                            tempstr1 = firstCol + ',point2';
                            this.paylineObject[tempstr1] = {
                                x: parseInt($(document.getElementById(tempstr1)).css('left'), 10),
                                y: parseInt($(document.getElementById(tempstr1)).css('top'), 10)
                            };
                            tempstr2 = secCol + ',point2';
                            this.paylineObject[tempstr2] = {
                                x: parseInt($(document.getElementById(tempstr2)).css('left'), 10),
                                y: parseInt($(document.getElementById(tempstr2)).css('top'), 10)
                            };
                        } else {
                            tempstr1 = firstCol + ',point2';
                            this.paylineObject[tempstr1] = {
                                x: parseInt($(document.getElementById(tempstr1)).css('left'), 10),
                                y: parseInt($(document.getElementById(tempstr1)).css('top'), 10)
                            };
                            tempstr2 = firstCol + ',point3';
                            this.paylineObject[tempstr2] = {
                                x: parseInt($(document.getElementById(tempstr2)).css('left'), 10),
                                y: parseInt($(document.getElementById(tempstr2)).css('top'), 10)
                            };
                            tempstr3 = secCol + ',point1';
                            this.paylineObject[tempstr3] = {
                                x: parseInt($(document.getElementById(tempstr3)).css('left'), 10),
                                y: parseInt($(document.getElementById(tempstr3)).css('top'), 10)
                            };
                            tempstr4 = secCol + ',point2';
                            this.paylineObject[tempstr4] = {
                                x: parseInt($(document.getElementById(tempstr4)).css('left'), 10),
                                y: parseInt($(document.getElementById(tempstr4)).css('top'), 10)
                            };
                        }
                    }
                }

                tempstr1 = 'column' + (this.highlightSymbol.length - 1) + ',symbol' + this.highlightSymbol[this.highlightSymbol.length - 1] + ',point3';
                this.paylineObject[tempstr1] = {
                    x: parseInt($(document.getElementById(tempstr1)).css('left'), 10),
                    y: parseInt($(document.getElementById(tempstr1)).css('top'), 10)
                };

                var keys = Object.keys(this.paylineObject);

                var payline = {};
                payline.width = "3";
                payline.color = '#a01ddf';
                payline.points = [];
                var typeMove = {};
                typeMove.type = 'Move';
                typeMove.point = [this.paylineObject[keys[0]]];
                payline.points.push(typeMove);

                var typeLine = {};
                typeLine.type = 'Line';
                typeLine.point = [];

                for (i = 1 ; i < keys.length ; i++) {
                    typeLine.point.push(this.paylineObject[keys[i]]);
                }
                payline.points.push(typeLine);

                this.drawPayLine(payline);
                paylineGeneratorNamespace.setupCheckBox.getResDimensions();
                this.createPointsforResolution();

                for (var res in this.allResPoints) {
                   var payline =  this.createPaylineMapping(this.allResPoints[res], keys);
                   paylineGeneratorNamespace.paylineGeneratorUtils.displayTxtCo(payline , res , 0);
                }
            } else {
                alert('PLEASE SELECT A RESOLUTION')
            }
        } else {
            alert('Please select a symbol from each row.');
        }
    };

    _p.createPointsforResolution = function () {
        this.allResPoints = {};
        this._visibleArea = {
            columns : this.setDisplayAreaData.columns,
            rows: this.setDisplayAreaData.rows
        }
        for (var x in paylineGeneratorNamespace.setupCheckBox._checkBoxObject) {
            this.allResPoints[x] = paylineGeneratorNamespace.paylineGeneratorUtils.CalculateResolutionSizes(x, this._visibleArea)[x];
        };
    };

    _p.createPaylineMapping = function (obj , dta) {
        var paylineObject = {};
        for (var x in dta) {
            paylineObject[dta[x]] = obj[dta[x]];
        }
        var keys = Object.keys(paylineObject);
        var payline = {};
        payline.points = [];
        var typeMove = {};
        typeMove.type = 'Move';
        typeMove.point = [paylineObject[keys[0]]];
        payline.points.push(typeMove);

        var typeLine = {};
        typeLine.type = 'Line';
        typeLine.point = [];

        for (i = 1 ; i < keys.length ; i++) {
            typeLine.point.push(paylineObject[keys[i]]);
        }
        payline.points.push(typeLine);
        return payline;
    };

    _p.getNumberObject = function (arg) {
        var temp = arg.split(',');
        return temp[0].substring(6) + temp[1].substring(6) + temp[2].substring(5);
    };

    _p.bubbleSort = function (a) {
        var swapped;
        do {
            swapped = false;
            for (var i = 0; i < a.length - 1; i++) {
                if (a[i] > a[i + 1]) {
                    var temp = a[i];
                    a[i] = a[i + 1];
                    a[i + 1] = temp;
                    swapped = true;
                }
            }
        } while (swapped);
    };

    _p.drawPayLine = function (data) {

        var canvas = document.getElementById('paylineCanvas');
        var offscreencontext = canvas.getContext("2d");

            offscreencontext.lineWidth = data.width;
            offscreencontext.strokeStyle = data.color;
            offscreencontext.beginPath();
            var len = data.points.length;
            for (var i = 0; i < len; i++) {
                var dta = data.points[i];
                switch (dta.type) {
                    case 'Move':
                        offscreencontext.moveTo(dta.point["0"].x, dta.point["0"].y);
                        break;
                    default:
                        for (var pointSet in dta.point) {
                            offscreencontext.lineTo(dta.point[pointSet].x, dta.point[pointSet].y);
                        }
                        break;
                }
            }
            offscreencontext.stroke();
            canvas.style.zIndex = '10';
    };

    paylineGeneratorNamespace.setDisplayArea = SetDisplayArea;

}(window));