﻿(function (global) {

    var paylineGeneratorNamespace = global.mgs.mobileAutomations.common.Utils.resolveNamespace('mgs.mobileAutomations.paylineGenerator');
    var commonUtils = global.mgs.mobileAutomations.common.Utils.resolveNamespace('mgs.mobileAutomations.common.Utils');
    function SetupCheckBox() {
        this._checkBoxObject = {};
        this._globalResObject = {};
        this.setup();
    };

    var _p = SetupCheckBox.prototype;

    _p.setup = function () {
        this.setUpResCheckBoxes();
        $("#ClearTxt").hide();
    };

    _p.setUpResCheckBoxes = function () {
        var key = Object.keys(paylineGeneratorNamespace.config.resolution);
        for (var k in key) {
            var el = document.createElement('div');
            el.setAttribute('class', 'checkbox');
            //el.setAttribute('float', 'left');
            var chkb = document.createElement('input');
            chkb.setAttribute('type', 'checkbox');
            chkb.setAttribute('id', key[k]);
            chkb.setAttribute('value', key[k]);

            var holder = document.createElement('div');
            holder.id = 'holder' + key[k];
            holder.setAttribute('class', 'holder');
            var symH = document.createElement('input');
            symH.setAttribute('type', 'number');
            symH.setAttribute('id', 'height' + key[k]);
            symH.setAttribute('value', paylineGeneratorNamespace.config.resolution[key[k]].symbolHeight);
            symH.setAttribute('min', '1');
            symH.setAttribute('required', 'true');
            symH.setAttribute('class', 'input-mini');

            var symW = document.createElement('input');
            symW.setAttribute('type', 'number');
            symW.setAttribute('id', 'width' + key[k]);
            symW.setAttribute('value', paylineGeneratorNamespace.config.resolution[key[k]].symbolWidth);
            symW.setAttribute('min', '1');
            symW.setAttribute('required', 'true');
            symW.setAttribute('class', 'input-mini');

            var symRD = document.createElement('input');
            symRD.setAttribute('type', 'number');
            symRD.setAttribute('id', 'reelDiv' + key[k]);
            symRD.setAttribute('value', paylineGeneratorNamespace.config.resolution[key[k]].reelDiv);
            symRD.setAttribute('min', '1');
            symRD.setAttribute('required', 'true');
           symRD.setAttribute('class', 'input-mini');
           // holder.onclick = commonUtils.createDelegate(this, function (e) {
             //   e.cancelBubble = true;
          //  });
            $(holder).click(function (event) {
                event.stopPropagation();
                event.cancelBubble = true;
            });
            holder.appendChild(symH);
            holder.appendChild(document.createTextNode('Symbol Height'));
            holder.appendChild(symW);
            holder.appendChild(document.createTextNode('Symbol Width'));
            holder.appendChild(symRD);
            holder.appendChild(document.createTextNode('Reel Div Space'));

            chkb.onchange = commonUtils.createDelegate(this, function (e) {
                $('#textlabel' + e.target.id).toggle();
                $('#holder' + e.target.id).toggle();
               // $('#holder' + e.target.id).show();
                if (this._checkBoxObject[e.target.id] === undefined) {
                    $("#ClearTxt").show();
                    this._checkBoxObject[e.target.id] = e.target.id
                    console.log(this._checkBoxObject);
                } else {
                    delete this._checkBoxObject[e.target.id];
                    if ($.isEmptyObject(this._checkBoxObject)) {
                        $("#ClearTxt").hide();
                    }
                    console.log(this._checkBoxObject);
                }
            });

            el.appendChild(chkb);
            el.appendChild(document.createTextNode(key[k]));
            el.appendChild(holder);
            $(holder).hide();
            $('#ResCheck').append(el);
            $("#ClearTxt").show();
            $("#ClearTxt").click(commonUtils.createDelegate(this, function (e) {
                var key = $('#display label textarea');
                for (var a = 0; a < key.length ; a++) {
                    $(key[a]).val('');
                }
            }));

            var txtLab = document.createElement('label');
            txtLab.setAttribute('id', 'textlabel' + key[k]);
            $(txtLab).css('width', $('#bodyContent').width() / 2 + 'px');
            $(txtLab).css('float', 'left');
            var txtel = document.createElement('textarea');
            txtel.setAttribute('id', 'textArea' + key[k]);
            txtel.setAttribute('class', 'txtbox');
            $(txtel).css('width', $('#bodyContent').width() / 2 + 'px');
            $(txtel).css('height', '200px');

            txtLab.appendChild(txtel);
            txtLab.appendChild(document.createTextNode(key[k] + ' points'));
            $('#display').append(txtLab);
            $(txtLab).hide();
        }
    };

    _p.getResDimensions = function () {
        var key = Object.keys(paylineGeneratorNamespace.setupCheckBox._checkBoxObject);
        this._finalResDimensions = {};
        for (var x in paylineGeneratorNamespace.setupCheckBox._checkBoxObject) {
            this._finalResDimensions[x] = {
                symbolWidth: parseInt( $('#width' + x).val()),
                symbolHeight: parseInt($('#height' + x).val()),
                reelDiv: parseInt($('#reelDiv' + x).val())
            }
        }
    };

    paylineGeneratorNamespace.setupCheckBox = new SetupCheckBox();
}(window));