﻿(function(global) {
    var paylineGeneratorNamespace = global.mgs.mobileAutomations.common.Utils.resolveNamespace('mgs.mobileAutomations.paylineGenerator');

    var config = {
        resolution: {
            '480x320': {
                symbolWidth: 82,
                symbolHeight: 82,
                reelDiv : 7
            },
            '960x640': {
                symbolWidth: 164,
                symbolHeight: 164,
                reelDiv: 14
            },
            '1024x768': {
                symbolWidth: 174,
                symbolHeight: 174,
                reelDiv: 13
            },
            '1136x640': {
                symbolWidth: 190,
                symbolHeight: 164,
                reelDiv: 14
            }
        }
    };

    paylineGeneratorNamespace.config = config;
    
})(window);