﻿(function (global) {

  var commonNameSpace;

  function Utils() {
  };
  
  /**
  * Inspired by O'REILLY JavaScript Patterns Page 89
  * Returns the object within the given and creates any objects that don't exist along the path.
  * @method Utils.resolveNamespace
  * @static
  * @param nameSpaceString The namespace path, for example, 'mgs.mobile.casino.v'
  * @return {String} A reference to the final object in the nameSpaceString
  */
  Utils.resolveNamespace = function (nameSpaceString) {
    var parts = nameSpaceString.split('.'),
        parent = global,
        i;

    for (i = 0; i < parts.length; i += 1) {
      //Create property if it doesn't exist
      if (typeof parent[parts[i]] === 'undefined') {
        parent[parts[i]] = {};
      }
      parent = parent[parts[i]];
    }
    return parent;
  };

  /**
  * Creates a method closure with scope
  */
  Utils.createDelegate = function (contextObject, delegateMethod) {
    return function () {
      return delegateMethod.apply(contextObject, arguments);
    };
  };
  // expose the type
  commonNameSpace = Utils.resolveNamespace('mgs.mobileAutomations.common');
  commonNameSpace.Utils = Utils;

} (window));