﻿(function (global) {

    var paylineGeneratorNamespace = global.mgs.mobileAutomations.common.Utils.resolveNamespace('mgs.mobileAutomations.paylineGenerator');
    var commonUtils = global.mgs.mobileAutomations.common.Utils.resolveNamespace('mgs.mobileAutomations.common.Utils');

    function uiView() {
        this._indexs = {};
        this.setUp();
    };

    var _p = uiView.prototype;

    _p.setUp = function () {
        var _config = paylineGeneratorNamespace.config;
        $('#chooseFile').change(commonUtils.createDelegate(this, function (e) {
            var files = e.target.files; // FileList object
            // Loop through the FileList
            for (var q = 0, f; f = files[q]; q++) {
                if (!f.type.match('xml.*')) {
                    alert("XML PLEASE");
                    continue;
                }

                var reader = new FileReader();
                reader.onload = (commonUtils.createDelegate(this, function (theFile) {
                    return commonUtils.createDelegate(this, function (evt) {
                        var count = 0;
                        $(evt.target.result).find('Payline').each(function () {
                            this._indexs['Payline' + count] = $(this).attr("indexes").split(',');
                            console.log($(this).attr("indexes").split(','));
                        });
                    });
                }))(f);
                var t = reader.readAsText(f);
            }
        }));

    };

    _p.createPaylinefromConfig = function (data) {
        var count = 0;
        $(data).find('Payline').each(function () {
            this._indexs['Payline' + count] = $(this).attr("indexes").split(',');
            console.log($(this).attr("indexes").split(','));
        });
    };


    paylineGeneratorNamespace.uiView = new uiView();
})(window);