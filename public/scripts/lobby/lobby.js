var dragSrcEl = null;
var orderArray = [];


function handleDragStart(e) {
  // Target (this) element is the source node.
  //this.style.opacity = '0.4';

  dragSrcEl = this;
  dragSrcEl.id = this.id;

  e.dataTransfer.effectAllowed = 'move';
  e.dataTransfer.setData('text/html', this.innerHTML);
  e.dataTransfer.setData('id', this.id);
  e.dataTransfer.setData("value", JSON.stringify({key: this.innerHTML}));
}

function handleDragOver(e) {
  if (e.preventDefault) {
    e.preventDefault(); // Necessary. Allows us to drop.
  }

  e.dataTransfer.dropEffect = 'move';  // See the section on the DataTransfer object.

  return false;
}

function handleDragEnter(e) {
  // this / e.target is the current hover target.
  this.classList.add('over');
}

function handleDragLeave(e) {
  this.classList.remove('over');  // this / e.target is previous target element.
}

function handleDrop(e) {
  // this/e.target is current target element.

  if (e.stopPropagation) {
    e.stopPropagation(); // Stops some browsers from redirecting.
  }
  
  // Set the ID that you're swapping with.
  dragSrcEl.id = this.id;
  
  // Don't do anything if dropping the same column we're dragging.
  if (dragSrcEl != this) {
    // Set the source column's HTML to the HTML of the column we dropped on.
    dragSrcEl.innerHTML = this.innerHTML;
    this.innerHTML = e.dataTransfer.getData('text/html');
    this.id = e.dataTransfer.getData('id');
  }
  getOrder();
  return false;

 
}

function getOrder() {
  orderArray = []; 
  $('#columns').children().each(function () {
    //console.log(this.id); // "this" is the current element in the loop
    orderArray.push(this.id);
  });
   
  /* get some values from elements on the page: */
    var term = orderArray,
      url = '/lobby-post';
 
  /* Send the data using post */
  var posting = $.post( url, { s: term } );
 
  return orderArray;
};

function handleDragEnd(e) {
  // this/e.target is the source node.
  [].forEach.call(cols, function (col) {
    col.classList.remove('over');
  });
}

var cols = document.querySelectorAll('#columns .column');
[].forEach.call(cols, function(col) {
  col.addEventListener('dragstart', handleDragStart, false);
  col.addEventListener('dragenter', handleDragEnter, false)
  col.addEventListener('dragover', handleDragOver, false);
  col.addEventListener('dragleave', handleDragLeave, false);
  col.addEventListener('drop', handleDrop, false);
  col.addEventListener('dragend', handleDragEnd, false);
});

$(document).ready(function(){

var cachedEl = $(".device-container").first();

function setSize() {
    if( $(cachedEl).find(".device-mockup").attr("data-orientation") == "portrait" ) {
        $(cachedEl).css("maxWidth", $(cachedEl).attr("data-size-port"));
    } else {
        $(cachedEl).css("maxWidth", $(cachedEl).attr("data-size-land"));
    }
}

$("#device-tabs > a").click(function(e){
    e.preventDefault();
    $(this).addClass("active").siblings().removeClass("active");
    $(cachedEl).attr("data-size-port", $(this).attr("data-size-port")).attr("data-size-land", $(this).attr("data-size-land")).find(".device-mockup").attr("data-device", $(this).attr("data-value"));
    setSize()
    $("#generated-code").text($(cachedEl).html());
    return false;
});

$("#orientation-tabs > a").click(function(e){
    e.preventDefault();
    $(this).addClass("active").siblings().removeClass("active");
    $(cachedEl).find(".device-mockup").attr("data-orientation", $(this).attr("data-value"));
    setSize()
    $("#generated-code").text($(cachedEl).html());
    return false;
});

$("#color-tabs > a").click(function(e){
    e.preventDefault();
    $(this).addClass("active").siblings().removeClass("active");
    $(cachedEl).find(".device-mockup").attr("data-color", $(this).attr("data-value"));
    $("#generated-code").text($(cachedEl).html());
    return false;
});

$("#generated-code").text($(cachedEl).html());

});