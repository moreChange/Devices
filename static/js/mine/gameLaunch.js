(function(global){

	function setup(){
		var host = global.document.getElementById('host');
		var lobbyHost = global.document.getElementById('lobbyHost');
		host.value = getIP();
		lobbyHost.value = getIP();

		$('#gameLaunchButton').on('click', gameLaunch);
		$('#lobbyLaunchButton').on('click', lobbyLaunch);
	}

	function getIP () {
		return global.location.hostname;
	}

	function gameLaunch () {
		var localHost, userName, game, resolution, topBar;
        var createURL;

		var host = global.document.getElementById('host');
		host.value = getIP();
		localHost = document.getElementById('host').value;
        userName = document.getElementById('userName').value;
        game = document.getElementById('game').value.split(',');
        resolution = document.getElementById('resolution').value;
        topBar = $('#enableTopBar').hasClass('active');
        password = document.getElementById('password').value;
        language = document.getElementById('languages').value;

		// Just doubling up on checking for host.
        if (localHost === null || localHost === '') {
            localHost = window.location.host;
        }

        // Insert your default userName if one is not provided.
        if (userName === null || userName === '') {
            userName = 'juanp';
        }

        // Insert your default password if one is not provided.
        if (password === null || password === '') {
            password = 'test';
        }

        // Minimum to run a game
        createURL = "http://" + localHost + "/MobileWebGames/game/?moduleID=" + game[1] + "&clientID="+game[2]+"&gameName=" + game[0] + "&gameTitle=" + game[0];
		createURL += "&LanguageCode="+ language +"&clientTypeID=40&casinoID=5001&lobbyName=skybet&loginType=InterimUPE&bankingURL=http%3A%2F%2F" + localHost +"";
		createURL += "%2FMobileWebLobby%2F%3F&xmanEndPoints=http%3A%2F%2F" + localHost;
		createURL += "%2FXManHandler%2Fx.x&routerEndPoints=&isPracticePlay=false&username=" + userName + "&password=" + password;



        // Include Resolution if selected is other than default.
		if (resolution !== 'default') {
		    createURL += "&resolution=" + resolution;
        }

        // Include topBar if selected.
		if(topBar) {
			createURL += "&topBar=GCM&channel=M&commonUIURL=/gcm-tests/gcm-example-commonui/commonui.html&disableSessionWebService=true&playMode=real";
        }
        window.location = createURL;
	}

	function lobbyLaunch () {
		var localHost, lobbyName, language

		localHost = document.getElementById('lobbyHost').value;
        lobbyName = document.getElementById('lobbyName').value;
        language = document.getElementById('lobbyLanguages').value;

        createURL = "http://" + localHost + "/MobileWebLobby/Main/Lobby?lobbyName="+ lobbyName +"&languageCode=" + language;

        window.location = createURL;
	}

	setup();
	
}(window));