// loads model file and engine
var mongoose    = require('mongoose'),
    gamesModel  = require('../models/GamesModel');
    gamesOrder  = require('../models/GamesOrder');

// Open DB connection
//mongoose.connect('mongodb://127.0.0.1:27017/lobbyOrder');

var feed = require('rsj');

exports.feeds = function (req,res){
	console.log('IN FEEDS');
	var hackerNews;

	feed.r2j('https://news.ycombinator.com/rss',function(data) { 
		var temp = JSON.parse(data);
		console.log('NOM NOM NOM : HACKER NEWS');
		hackerNews = temp;
		console.log(hackerNews);
	});

	var apple;

	feed.r2j('http://www.geek.com/articles/apple/feed/',function(data) { 
		var temp = JSON.parse(data);
		console.log('NOM NOM NOM : Apple NEWS');
		apple = temp;
	});
}

exports.feedsApple = function (req,res){

	var apple;

	feed.r2j('http://www.geek.com/articles/apple/feed/',function(data) { 
		var temp = JSON.parse(data);
		console.log('NOM NOM NOM : Apple NEWS');
		apple = temp;

	});

	res.write(apple);
	res.end();

}

// Index Page.
exports.index = function(req, res){
	res.render('index', { title: 'Home' });	
};

// Game Launch
exports.launch = function(req, res){
	res.render('game-launch', { title: 'Game Launcher' });
};

// Device Compatibility
exports.device = function(req, res){
  gamesModel.find({'device.deviceNo':35}, function(err, games){
  		res.render('gamecomp', { title: 'Game Compatibility', games: games });
	});
};

// Device Compatibility
exports.lobby = function(req, res){
	gamesOrder.find(function(err, orders){
		
		orders.sort(function(a, b) { // Simple sort function on orderNum
 			return a.orderNum-b.orderNum
		});

  		res.render('lobby-order', { title: 'Game Order', gameOrder: orders });
	});
};

// Device Compatibility
exports.lobby_post = function(req, res) {
  saveOrder(req.body.s);
  res.write('Saved');
  res.end();
 
};

// Device Compatibility
exports.game_post = function(req, res){
  res.redirect('/game-comp');
};

// Paylines
exports.paylines = function(req, res){
  res.render('paylines', { title: 'Payline Generator' });
};

function saveOrder(data){
	var fs = require('fs');
	var tmpString = 'USE Mobile'+String.fromCharCode(13)+
					'GO'+String.fromCharCode(13); // Used in the Beginning

	var game = [];
	var gameName = '';
	var gameMid = '';
	var gameCid = '';

	var buildString = ''; // This is the sql building blocks
	for (var i = 0; i < data.length; i++) {
		game = data[i].split(',');
		gameName = game[0];
		gameMid = game[1];
		gameCid = game[2];

		// Kewl, lets update the DB :).
		gamesOrder.update({ imageName:gameName }, { orderNum: i } , {upsert: false},function (err, doc){
		  if(err) console.log(err);
		});

		if (i === 0) { // this is no buenos
			buildString += tmpString + '		UPDATE tb_GameCategoryGame SET POSITION = ' + (i+1) + ' WHERE ModuleID = ' + gameMid + ' AND ClientID = ' + gameCid + ' --' + gameName + String.fromCharCode(13);
		} else {
			buildString += '		UPDATE tb_GameCategoryGame SET POSITION = ' + (i+1) + ' WHERE ModuleID = ' + gameMid + ' AND ClientID = ' + gameCid + ' --' + gameName +String.fromCharCode(13)
		}

		  
	}; // End appendFile

	// The flag 'w' 
	fs.writeFile("/tmp/genericLobby.sql",	buildString + String.fromCharCode(13),{ flag : 'w'}, 
		function(err) {
			if(err) {
	        	console.log(err);
		    } else {
		        console.log("The file was saved!");
		    }
	});	 
	
};
